*** Settings ***
Documentation  A page object describing elements of site header (except the nav menu)
Library  Selenium2Library
Resource  Utils/Element.robot

*** Variables ***
# A CSS locator for the search field
${MAIN_SEARCH_FIELD_LOCATOR}=  css=form.search input

*** Keywords ***

Search On Site For Text
    [Documentation]  Accepts 1 argument - the text to enter to the search field in the site header.
    [Arguments]  ${search_text}
    # Make the search field focused
    Wait And Click Element  ${MAIN_SEARCH_FIELD_LOCATOR}
    # Type text passed as argument to this keyword
    Input Text  ${MAIN_SEARCH_FIELD_LOCATOR}  ${search_text}
    # Press Enter within the field
    Press Key  ${MAIN_SEARCH_FIELD_LOCATOR}  \\13

