*** Settings ***
Documentation  Example test for searching on the page
Library  Selenium2Library
Library  StepsLibrary
Force Tags  search


*** Test Cases ***

Search by single word
    [Tags]  TC001
    When In page header I search for text  Selenium
    Then On Search Results pane I should see a link with text  Test Automation
    When On Search Results pane I click a link with text  Test Automation
    Then I should see page title  Test Automation
