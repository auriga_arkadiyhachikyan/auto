from robot.libraries.BuiltIn import BuiltIn


class Element:
    """
    A class to implement static methods helping to work with HTML elements.
    This class uses keywords from the existing instance of Selenium2Library.
    """

    """
    A field to remember the instance of Selenium2Library used by the Robot Framework
    """
    _selenium_lib = None

    def __init__(self):
        pass

    @staticmethod
    def get_selenium2_library():
        """
        Receives the current running instance of Selenium2Library object.
        You can use it to retrieve the current WebDriver object or call any keywords.
        :return: The instance of Selenium2Library
        """
        if Element._selenium_lib is None:
            Element._selenium_lib = BuiltIn().get_library_instance("Selenium2Library")
        return Element._selenium_lib

    @staticmethod
    def get_driver():
        """
        Gets an instanse of Selenium driver that Selenium2Library is currently using
        :return: A WebDriver object
        """
        return Element.get_selenium2_library()._current_browser()

    @staticmethod
    def wait_for_element(locator):
        """
        Waits until page contains the element, then returns this element.
        :param locator: Locator in format required by Selenium2Library (e.g., xpath=..., css=...)
        :return: The instance of WebElement if present, None if not found
        """
        try:
            lib = Element.get_selenium2_library()
            lib.wait_until_page_contains_element(locator)
            return lib.get_webelement(locator)
        except AssertionError:
            return None

    @staticmethod
    def wait_and_click_element(locator):
        """
        Waits until the element is visible, then simulates a mouse click on it.
        :param locator: Locator in format required by Selenium2Library (e.g., xpath=..., css=...)
        """
        lib = Element.get_selenium2_library()
        lib.wait_until_element_is_visible(locator)
        return lib.click_element(locator)

    @staticmethod
    def mouse_over(locator):
        """
        Waits until the element is visible, then simulates holding mouse over it.
        :param locator: Locator in format required by Selenium2Library (e.g., xpath=..., css=...)
        """
        lib = Element.get_selenium2_library()
        lib.wait_until_element_is_visible(locator)
        lib.mouse_over(locator)

    @staticmethod
    def get_attribute_of_element(locator, attribute_name):
        """
        Waits until page contains the element, then returns a required attribute.
        :param locator: Locator in format required by Selenium2Library (e.g., xpath=..., css=...)
        :param attribute_name: name of the attribute in the HTML tag
        :return: value of the attribute
        """
        try:
            element = Element.wait_for_element(locator)
            return element.get_attribute(attribute_name)
        except AssertionError:
            return ""

    @staticmethod
    def get_visibility_of_element(locator):
        """
        Waits until page contains the element, then finds out its visibility.
        :param locator: Locator in format required by Selenium2Library (e.g., xpath=..., css=...)
        :return: True or False
        """
        try:
            element = Element.wait_for_element(locator)
            return element.is_displayed()
        except AssertionError:
            return False
