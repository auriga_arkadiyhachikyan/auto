from pageobjects.SiteHeader import SiteHeader
from pageobjects.SearchResultsPage import SearchResultsPage


class _SearchSteps(object):
    """
    Steps for the Search activities
    """
    def __init__(self):
        pass

    def in_page_header_i_search_for_text(self, text):
        print "Searching for text: " + text
        SiteHeader.search_on_site_for_text(text)

    def on_search_results_pane_i_should_see_a_link_with_text(self, text):
        visible = SearchResultsPage.get_search_link_visibility_by_text(text)
        if not visible:
            error = "Link '" + text + "' is not shown on screen!"
            print error
            raise RuntimeError(error)

    def on_search_results_pane_i_click_a_link_with_text(self, text):
        print "Clicking a link: " + text
        SearchResultsPage.click_search_link_by_text(text)
