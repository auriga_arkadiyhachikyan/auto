package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import helper.Screenshooter;
import helper.StepsBase;
import pages.Home;

/**
 * Class to describe common hooks for scenarios.
 * Annotations @Before and @After are to mark steps that will be executed before and after each Scenario.
 */
public class Hooks extends StepsBase {

    @Before
    public void beforeScenario(Scenario scenario) throws Throwable {
        logger.info("\n>>>>> STARTED TEST: " + scenario.getName() + " <<<<<\n");

        setUpDriver();

        // Reset the application for a new test
        new Home(driver, wait).closeInitialDialogIfDisplayed();
    }

    @After
    public void afterScenario(Scenario scenario) throws Throwable {
        if (!scenario.isFailed()) {
            logger.info("\n>>>>> PASSED TEST: " + scenario.getName() + " <<<<<\n");
        } else {
            String screenshotPath = Screenshooter.takeScreenshot(driver);
            logger.error("Saved screenshot at: " + screenshotPath);
            logger.info("\n>>>>> FAILED TEST: " + scenario.getName() + " <<<<<\n");
        }
        // Closing the Appium session
        tearDownDriver();
    }

}
