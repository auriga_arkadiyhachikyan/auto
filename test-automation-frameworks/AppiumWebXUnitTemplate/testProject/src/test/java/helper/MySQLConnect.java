package helper;


import java.sql.*;

//Class to work with MySQL database using jdbc driver. Performs connection to MySQL DB and getting result of query
//Note that this class works only for MySQL DB but easily could be modified to work with other DB
public class MySQLConnect {

    private Connection con = null;

    public MySQLConnect(String url, String port, String dbName, String login, String pass) {
        setConnection(url, port, dbName, login, pass);
    }

    public void setConnection(String url, String port, String dbName, String login, String pass) {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://" + url + ":" + port + "/" + dbName, login, pass);
        }
        catch (ClassNotFoundException ex)
        {
            System.err.println("Cannot find this db driver classes. Install com.mysql.jdbc driver!");
        }
        catch (SQLException e)
        {
            System.err.println("Cannot connect to " + dbName);
        }
        if (con != null)
        {
            System.out.println("Connection to DB established successfully.");
        }
    }


    public String getQueryResult(String query, String columnLabel)
    {
        try
        {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);
            rs.next();
            String result = rs.getString(columnLabel);
            rs.close();
            st.close();
            return result;
        }
        catch (SQLException e)
        {
            System.err.println(e);
            return null;
        }
    }
}