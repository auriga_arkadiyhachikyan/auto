package helper;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import java.awt.*;

/**
 * A class that is used to transform coordinates of Web Elements from CSS units to physical pixels on device
 */
public class WebViewTransform {

    /**
     * Gets size of the displayed region in CSS units (should be in web context)
     */
    public static Dimension getWebViewCSSSize(AppiumDriver driver) {
        WebElement body = driver.findElement(By.xpath("//body"));
        return body.getSize();
    }

    /**
     * Gets size and location of an element in units that apply to current context
     */
    public static Rectangle getElementGeometry(WebElement element) {
        Point location = element.getLocation();
        Dimension size = element.getSize();
        return new Rectangle(
                location.getX(), location.getY(),
                size.getWidth(), size.getHeight());
    }

    /**
     * Gets size and location of a WebView element (should be in native context)
     */
    public static Rectangle getWebViewNativeGeometry(AppiumDriver driver) {
        WebElement webView = driver.findElement(By.xpath("//android.webkit.WebView"));
        return getElementGeometry(webView);
    }

    /**
     * Calculates size and position of an element depending on its CSS coordinates and WebView geometry data.
     *
     * @param webElementCSSGeometry Size and position of a WebElement in CSS units
     * @param webViewCSSSize       Size of WebView in CSS units
     * @param webViewNativeGeometry    Size and position of WebView in native pixels
     */
    public static Rectangle getElementNativeGeometry(Rectangle webElementCSSGeometry,
                                                     Dimension webViewCSSSize, Rectangle webViewNativeGeometry) {
        double hScale = webViewNativeGeometry.getWidth() / webViewCSSSize.getWidth();
        double vScale = webViewNativeGeometry.getHeight() / webViewCSSSize.getHeight();
        return new Rectangle(
                (int) (webElementCSSGeometry.getX() * hScale + webViewNativeGeometry.getX()),
                (int) (webElementCSSGeometry.getY() * vScale + webViewNativeGeometry.getY()),
                (int) (webElementCSSGeometry.getWidth() * hScale),
                (int) (webElementCSSGeometry.getHeight() * vScale));
    }

    /**
     * Calculates the center point of a rectangle
     */
    public static Point getCenter(Rectangle rect) {
        return new Point(
                (int) (rect.getX() + rect.getWidth() / 2),
                (int) (rect.getY() + rect.getHeight() / 2));
    }

}
