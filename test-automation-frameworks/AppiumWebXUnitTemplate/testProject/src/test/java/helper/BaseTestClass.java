package helper;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.Common;

/**
 * Class to describe actions to perform before and after each test.
 * Annotations @Before and @After are standard for JUnit to perform before and after the test.
 */
public class BaseTestClass {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected static final Logger logger = LogManager.getLogger(BaseTestClass.class);

    /**
     * Get browser name from the command line from "browser" parameter
     * Note that Firefox is the default browser. Firefox will be used if you have not set any parameter in command line
     */
    public static String driverName = System.getProperty("browser");

    @Before
    public void setUp() throws Exception {
        // Initialize WebDriver for the desired browser
        if (driverName == null) {
            driverName = "Firefox";
        }
        driver = BrowserFactory.getNewWebDriver(driverName);

        // Set timeout for waiting for element state (present or visible).
        // See details here http://docs.seleniumhq.org/docs/04_webdriver_advanced.jsp#explicit-and-implicit-waits
        wait = new WebDriverWait(driver, 30);

        // Use Common page object to open the home page
        new Common(driver, wait).openMainPage();
    }

    @After
    public void tearDown() throws Exception {
        // Not closing browser here because actions on fail and success are overridden in Rule and also close the browser
        // driver.quit();
    }

    @Rule
    public TestWatcher watchman = new TestWatcher() {
        //Override actions on fail to make screenshots and do other actions
        @Override
        protected void failed(Throwable e, Description description) {
            String screenshotPath = Screenshooter.takeScreenshot(driver);
            logger.error("Saved screenshot at: " + screenshotPath);
            logger.info("Test '" + description.getMethodName() + "' is Failed. Closing browser...");
            driver.quit();
        }

        //Override actions on success to report the passing test name and close the browser
        @Override
        protected void succeeded(Description description) {
            logger.info("Test '" + description.getMethodName() + "' is Passed. Closing browser...");
            driver.quit();
        }
    };
}
