package stepDefinitions;

import cucumber.api.java.en.Given;
import org.apache.log4j.Logger;

/**
 * Created by arkadiy.hachikyan on 12/2/2015.
 */
public class LeftPanel extends pages.LeftPanel{
//    Core core = Core.getInstance();
    public static final Logger logger = Logger.getLogger(LeftPanel.class);

    @Given("^on Left Panel I click Index link$")
    public void onLeftPanelIClickIndexLink() throws Throwable {
        logger.info("Click by Index Link");
        getIndexLink().click();
    }
}
