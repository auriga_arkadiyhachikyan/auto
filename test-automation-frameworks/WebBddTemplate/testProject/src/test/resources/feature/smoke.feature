@smoke
  Feature: Smoke tests

    @TC111
    Scenario: Verify Index Page content
      When on Left Panel I click Index link
      Then on any page I see that header is "Википедия:Алфавитный указатель"
      And on Index Page I see that navigation element "А" is presented
      And on Index Page I see that navigation element "БА—БЯ" is presented
      And on Index Page I see that navigation element "Ва" is presented

    @TC2222
    Scenario: Search for existing article
      When on Top Panel I search for "Тестирование программного обеспечения"
      Then on any page I see that header is "Тестирование программного обеспечения"

    @TC333
    Scenario: Failed search for existing article
      When on Top Panel I search for "Тестирование программного обеспечения"
      Then on any page I see that header is "Тести1231231"